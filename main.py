import os
from sizing import createDirectory, severalCrop
from manipulate_files import renameImagesDir

DIR_INPUT = './data/images'
DIR_OUTPUT = './data/output'
if __name__ == '__main__':
	
	# renomeia arquivos de um diretório.
	renameImagesDir(DIR_INPUT, 'lagarta')
	
	height = 1000 # altura das novas imagens
	width = 1000 # largura das novas imagens
	exact_size = True # caso queira somente imagens de tamanho informado em height e width
	createDirectory(DIR_OUTPUT)
	for path_img in os.listdir(DIR_INPUT):
		pth = DIR_INPUT + '/' + path_img
		severalCrop(pth, DIR_OUTPUT, height, width, exact_size) #cria várias novas imagens
	
