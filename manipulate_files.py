import os

'''
Renomeia os arquivos de um diretório
path: diretório onde estão os arquivos
prefixo: prefixo a ser aplicado no novo nome dos arquivos.
'''
def renameImagesDir(path, prefixo):
	files = os.listdir(path)
	for index, file in enumerate(files):
		name, extension = os.path.splitext(file)
		file_dst = prefixo+'_'+str(index)+extension
		path_dst = os.path.join(path, file_dst)
		path_src = os.path.join(path, file)
		print(path_src +' - '+ path_dst)
		os.rename(os.path.join(path, file), path_dst)
