import os
import cv2

'''
Cria um novo diretório.
'''
def createDirectory(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
'''
Divide a imagem em uma grade e salva cada célula da grade como uma nova imagem.
path: caminho da imagem (caminho + nome arquivo)
path_out: somente o caminho onde serão salvas as novas imagens.
height: altura das novas imagens
width: largura das novas imagens
exact_size: Se True somente imagens com os tamanhos definidos em height, width serão criadas. Caso
contrário, outras imagens com diferentes tamanhos serão criadas (sobras de height e width).
'''
def severalCrop(path, path_out, height, width, exact_size=True):
	image = cv2.imread(path)
	base_name = os.path.basename(path)
	name, extension = os.path.splitext(base_name)
	y=0
	x=0
	imgheight, imgwidth, channel = image.shape
	if height > imgheight or width > imgwidth:
		print('Imagem com tamanho inferior a ' + str(height) +'x'+ str(width))
		return
	y1 = 0
	M = height
	N = width
	idx = 1
	for y in range(0,imgheight,M):
		for x in range(0, imgwidth, N):
			y1 = y + M
			x1 = x + N
			if y1 > imgheight:
				if exact_size:
					continue
				else:
					y1 = imgheight
			if x1 > imgwidth:
				if exact_size:
					continue
				else:
					x1 = imgwidth
			tiles = image[y:y+M,x:x+N]
			new_image = path_out+'/'+name+'_crop_'+ str(idx)+extension
			cv2.imwrite(new_image,tiles)
			idx += 1